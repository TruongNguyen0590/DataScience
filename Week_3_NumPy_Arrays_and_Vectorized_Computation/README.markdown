# Week 3: Numpy

## Category

1. The Numpy ndarray: A Multidimensional Array Object
    - Creating ndarray - N-dimensional array
    - Some useful functions for creating new special arrays
    - Arithmetic with NumpyArray
    - Basic Indexing and Slicing
    - Indexing with slices
2. Comparision Operators
3. Boolean Arrays
    - Boolean Indexing
    - Fancy Indexing
4. Universal Function
    - Array Arithmetic
    - Absolute value
    - Trigonometric functions
    - Exponents and logarithms
5. Aggregations: Sum, Min, Max
    - Summing the Values in an Array
    - Min and Max
    - Multi dimensional aggregates
    - Sorting
    - Sorting along rows or columns
6. Homework
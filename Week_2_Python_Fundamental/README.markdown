# Week 2: Python Fundamental
- github: https://github.com/quangddt/Python_for_data_science

## Category
1. Language Sematics
    - Indentation, no braces
    - Comment
    - Variables and argument passing
    - Dynamic references
    - Attributes and methods
    - Import
    - Binary operators and comparisions
    - Scalar Types
    - Numeric types
    - Strings
    - Booleans
    - Type casting
2. Control Flow
    - if, elif and else
    - for loop
    - while loop
    - Tenary expression
3. Built-in Data Structures, Functions, and Files
    - Data Structures and Sequences
        - Tuple
            - Unpacking tuples
        - List
            - Adding and removing elements
            - Concatenating and combining list
            - Sorting
            - Slicing
        - Built-in Sequence Function
            - enumerate
            - reversed
            - zip
        - Dict
            - Creating dicts from sequences
            - Default values
            - Valid dict key type
        - Set
        - List, Set, and Dict Comprehension
    - Functions
        - Namespaces, Scope, and Local Functions
        - Return multiple values
        - Functions are objects
        - Lambda functions
        - Generators
            - Generator expression
        - Errors and Exception Handling
4. Homework
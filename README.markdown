# Data Science with Python - Course 1 - Syllabus

## 1. Week 1: 

- Giới thiệu về khóa học: instructors, students, syllabus, policy, 
expectations etc.
- Giới thiệu về Data Science
- Giới thiệu về Python
- Giới thiệu về Jupyter Notebook
- Exercise 1

## 2. Week 2

- Python programming language
- Exercise 2

## 3. Week 3 

- Python fundamental scientific computing package: Numpy
- Python data analysis library: Pandas	
- Exercise 3

## 4. Week 4

- Data structures in Data Science
- DataFrames in Pandas
- Exercise 4

## 5. Week 5

- Statistical techniques in Data Science
- Different data techniques in Data Science
- Exercise 5

## 6. Week 6

- Final project assignment
- Q&A

## 7. Week 7

- Data visualization in Data Science
- Publicly accessible datasets for Data Science
- Exercise 6

## 6. Week 8

- Machine learning 1 in Data Science
- Scikit-learn
- Exercise 7

## 9. Week 9

- Machine learning 2 in Data Science
- Scikit-learn
- Exercise 8

## 10. Week 10

- Final project presentation
- Course wrap-up

# Thông tin về người hướng dẫn:

`TS. Nguyễn Xuân Hà` : 
- Ha X. Nguyen received the B.Eng. degree from Posts and Telecommunications 
Institute of Technology, Ho Chi Minh City, Vietnam, in 2003, 
- the M.Sc. degree from Korea Advanced Institute of Science and Technology, 
Daejeon, Korea, in 2007,
- the Ph.D. degree from the University of Saskatchewan, Saskatoon, SK, 
Canada, in 2011. 
- From 2012 to 2016, he joined Complex System Inc., Calgary, AB to develop 
and implement a computer-vision-based human behavior recognition system. 
- Then he joined Wiivv Wearable Inc., Vancouver, BC for one year to develop 
algorithms to generate arch curves and foot length using images taken from 
mobile devices so a custom-fit insole/sandal can be designed and made. 
- Currently he is at Ambyint Inc. as a Data Scientist. He builds different 
artificial intelligence models for automation and optimization in oil and gas
 field. His research interests span the areas of artificial intelligence, 
 Internet of Things, computer vision and big data.

`TS. Tran Vu Khanh`: He is a mathematician and an AI expert. 
- He had worked for the University of Padova (Italy), Tan Tao University 
(Vietnam) and the National University of Singapore (Singapore). 
- He currently works at University of Wollongong (Australia) as a 
Vice-Chancellor’s Research Fellow/ARC DECRA Fellow.

`TS. Cao Tien Dung`: 
- Dr. Dung received PhD in computer science at University of Bordeaux, France
 (2010). 
- He currently works at Tan Tao University (Vietnam) as Lecturer and Vice-Dean
 of School of Engineering. 
- His research Interests: IoT, Big Data, Software testing, SOA, Service 
Engineering.

`Thạc sỹ Quang Duong`: 
- Quang Duong received M.Sc. degree in Electrical Engineering from University
 of Saskatchewan, Saskatoon, SK, Canada, in 2018.
- He is currently a Data Scientist at FDM Group. He works with FDM’s clients 
in financial and banking industry to design, develop and test a large-scale, 
custom distributed software system using the latest Python, Java and Big data
 technologies. His research interest includes big financial data, and 
 algorithmic trading.
